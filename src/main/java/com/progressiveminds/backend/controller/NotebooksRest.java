package com.progressiveminds.backend.controller;

import com.progressiveminds.backend.model.Notebooks;
import com.progressiveminds.backend.service.NotebooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/notes")
public class NotebooksRest {
	@Autowired
	private NotebooksService notebooksService;
	@CrossOrigin(origins = "*")
	@GetMapping( "lists" )
	public List<Notebooks> findAll() {
		return notebooksService.findAll();
	}

	@CrossOrigin(origins = "http//localhost:4200")
	@PostMapping("getbyid")
	public Notebooks getById(@RequestBody Integer notebookId ){
		System.out.print( notebookId );
		return notebooksService.findById( notebookId );
	}

	@GetMapping("list/{title}")
	public List<Notebooks> findByTitle( @PathVariable String title ) {
		return notebooksService.findByTitle( title );
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("list")
	public Notebooks save(@RequestBody Notebooks notebooks) {
		notebooksService.save(notebooks);
		return notebooks;
	}
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("delete")
	public Notebooks delete(@RequestBody long notebookId ){
		Notebooks tmp = notebooksService.findById( notebookId );
		notebooksService.delete( notebookId );
		return tmp;
	}
}
