package com.progressiveminds.backend.dao;

import com.progressiveminds.backend.model.Notes;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

@Repository
public interface NotesDao extends JpaRepository< Notes, Long >{

    List<Notes> findByTitle(String title);
    //	List<Product> findById( Long id );
    Notes findByNotebookId(long notebookId);

    List<Notes> findAll();
}
