package com.progressiveminds.backend.dao;


import com.progressiveminds.backend.model.Notebooks;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;


@Repository
public interface NotebooksDao extends JpaRepository<Notebooks, Long > {
	List<Notebooks> findByTitle(String title);
//	List<Product> findById( Long id );
	Notebooks findByNotebookId(long notebookId);

	List<Notebooks> findAll();

//	Product delete(long id);

//	Product findOne( int id );
}
