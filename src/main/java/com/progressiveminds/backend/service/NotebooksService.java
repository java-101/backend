package com.progressiveminds.backend.service;

import com.progressiveminds.backend.dao.NotebooksDao;
import com.progressiveminds.backend.model.Notebooks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class NotebooksService {
	@Autowired
	private NotebooksDao notebooksDao;

	public void save( Notebooks notebook ) {
		notebooksDao.save( notebook );
	}

	public List<Notebooks> findByTitle( String title ) {
		return notebooksDao.findByTitle( title );
	}

	public List<Notebooks> findAll() {
		return notebooksDao.findAll();
	}

	public void delete( long notebookId ) {
		notebooksDao.delete( notebookId );
	}

	public Notebooks findById( long notebookId ){
		return notebooksDao.findByNotebookId( notebookId );
	}

}
