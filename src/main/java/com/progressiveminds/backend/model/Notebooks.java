package com.progressiveminds.backend.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "notebooks")
public class Notebooks implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long notebookId;

	private String title;
	private Integer userId;
	private String description;


	public long getNotebookId() {
		return notebookId;
	}

	public void setNotebookId(long notebookId) {
		this.notebookId = notebookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
